/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;

/**
 *
 * @author long4
 */
public class Feedback {
    private int feedbackId;
    private User userId;
    private Order_Details orderDetailsId;
    private float ratedStar;
    private Product productId;
    private int feedbackStatus;
    private Date updateDate;
    private String tittle;
    private String note;

    public Feedback() {
    }

    public Feedback(int feedbackId, User userId, Order_Details orderDetailsId, int ratedStar, Product productId, int feedbackStatus, Date updateDate, String tittle, String note) {
        this.feedbackId = feedbackId;
        this.userId = userId;
        this.orderDetailsId = orderDetailsId;
        this.ratedStar = ratedStar;
        this.productId = productId;
        this.feedbackStatus = feedbackStatus;
        this.updateDate = updateDate;
        this.tittle = tittle;
        this.note = note;
    }

    public int getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(int feedbackId) {
        this.feedbackId = feedbackId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Order_Details getOrderDetailsId() {
        return orderDetailsId;
    }

    public void setOrderDetailsId(Order_Details orderDetailsId) {
        this.orderDetailsId = orderDetailsId;
    }

    public float getRatedStar() {
        return ratedStar;
    }

    public void setRatedStar(float ratedStar) {
        this.ratedStar = ratedStar;
    }

    public Product getProductId() {
        return productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }

    public int getFeedbackStatus() {
        return feedbackStatus;
    }

    public void setFeedbackStatus(int feedbackStatus) {
        this.feedbackStatus = feedbackStatus;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Feedback{" + "feedbackId=" + feedbackId + ", userId=" + userId + ", orderDetailsId=" + orderDetailsId + ", ratedStar=" + ratedStar + ", productId=" + productId + ", feedbackStatus=" + feedbackStatus + ", updateDate=" + updateDate + ", tittle=" + tittle + ", note=" + note + '}';
    }
    
    
}
