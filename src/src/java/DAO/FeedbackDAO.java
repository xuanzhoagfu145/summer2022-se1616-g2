/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Feedback;
import Model.Product;
import Model.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author long4
 */
public class FeedbackDAO extends DBContext {
    
    /**
     * set data for feedback of each product 
     *
     * @param pid
     * @return
     */
    public List<Feedback> getProductFeedback(int pid) {
        String sql = "select f.RatedStar, f.UpdateDate, f.Note, u.Name, u.Image from Feedback f inner join [User] u\n"
                + "on f.UserId = u.Id\n"
                + "where f.ProductId = ?";
        List<Feedback> ls = new ArrayList<>();
        //check preparedStatement not be failed
        try {

            PreparedStatement stm = connection.prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            stm.setInt(1, pid);
            ResultSet rs = stm.executeQuery();
            // loop all Result set to excute query and set data for feedback of each product
            while (rs.next()) {
                Feedback feedback = new Feedback();
                feedback.setRatedStar(rs.getFloat("ratedStar"));
                feedback.setUpdateDate(rs.getDate("updateDate"));
                feedback.setNote(rs.getString("note"));
                User users = new User();
                users.setName(rs.getString("name"));
                users.setImage(rs.getString("image"));
                feedback.setUserId(users);
                ls.add(feedback);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ls;
    }
    
    /**
     * get the average of rating of product
     *
     * @param productId
     * @return
     */
    public float getAvgRateOfProduct(int productId) {
        String sql = "SELECT AVG(RatedStar)\n"
                + "FROM Feedback\n"
                + "WHERE ProductId=?";
        //check preparedStatement not be failed
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, productId);
            ResultSet rs = stm.executeQuery();
            // loop all Result set to excute query and get the average of rating of product
            while (rs.next()) {
                return rs.getFloat(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public static void main(String[] args) {
        FeedbackDAO p = new FeedbackDAO();
//        List<Feedback> pt = p.getProductFeedback(1);
//        for (Feedback post_Category : pt) {
//            System.out.println(post_Category);
//        }
        float fb =p.getAvgRateOfProduct(1);
            System.out.println(fb);
    }
}
