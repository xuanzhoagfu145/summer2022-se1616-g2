<%-- 
    Document   : UserList2
    Created on : Jun 14, 2022, 5:48:58 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html>
<html lang="en">

    <head>

        <title>Flash Able - Most Trusted Admin Template</title>
        <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 11]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
        <!-- Meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="description"
              content="Flash Able Bootstrap admin template made using Bootstrap 4 and it has huge amount of ready made feature, UI components, pages which completely fulfills any dashboard needs." />
        <meta name="keywords"
              content="admin templates, bootstrap admin templates, bootstrap 4, dashboard, dashboard templets, sass admin templets, html admin templates, responsive, bootstrap admin templates free download,premium bootstrap admin templates, Flash Able, Flash Able bootstrap admin template">
        <meta name="author" content="Codedthemes" />

        <!-- Favicon icon -->
        <link rel="icon" href="/src/assests/img/favicon.ico" type="image/x-icon">
        <!-- fontawesome icon -->
        <link rel="stylesheet" href="/src/assests/plugins/font-awesome/css/font-awesome.min.css">
        <!-- animation css -->
        <link rel="stylesheet" href="/src/assests/plugins/animation/css/animate.min.css">
        <!-- vendor css -->
        <link rel="stylesheet" href="/src/assests/css/style_admin.css">


        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.min.css">

        <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
    </head>
    <style>.custom-select{
            margin-left: 30px;
        }
    </style>
    <body class="">
        <!-- [ Pre-loader ] start -->
        <div class="loader-bg">
            <div class="loader-track">
                <div class="loader-fill"></div>
            </div>
        </div>
        <!-- [ Pre-loader ] End -->
        <!-- [ navigation menu ] start -->
        <nav class="pcoded-navbar menupos-fixed menu-light brand-blue ">
            <div class="navbar-wrapper ">
                <div class="navbar-brand header-logo">
                    <a href="index.html" class="b-brand">
                        <img src="../assets/images/logo.svg" alt="" class="logo images">
                        <img src="../assets/images/logo-icon.svg" alt="" class="logo-thumb images">
                    </a>
                    <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
                </div>
                <div class="navbar-content scroll-div">
                    <ul class="nav pcoded-inner-navbar">
                        <li class="nav-item pcoded-menu-caption">
                            <label>Navigation</label>
                        </li>
                        <li class="nav-item">
                            <a href="index.html" class="nav-link"><span class="pcoded-micon"><i
                                        class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
                        </li>
                        <li class="nav-item pcoded-menu-caption">
                            <label>UI Element</label>
                        </li>
                        <li class="nav-item pcoded-hasmenu">
                            <a href="#!" class="nav-link"><span class="pcoded-micon"><i
                                        class="feather icon-box"></i></span><span class="pcoded-mtext">Componant</span></a>
                            <ul class="pcoded-submenu">
                                <li class=""><a href="bc_button.html" class="">Button</a></li>
                                <li class=""><a href="bc_badges.html" class="">Badges</a></li>
                                <li class=""><a href="bc_breadcrumb-pagination.html" class="">Breadcrumb & paggination</a>
                                </li>
                                <li class=""><a href="bc_collapse.html" class="">Collapse</a></li>
                                <li class=""><a href="bc_progress.html" class="">Progress</a></li>
                                <li class=""><a href="bc_tabs.html" class="">Tabs & pills</a></li>
                                <li class=""><a href="bc_typography.html" class="">Typography</a></li>
                            </ul>
                        </li>
                        <li class="nav-item pcoded-menu-caption">
                            <label>Forms &amp; table</label>
                        </li>
                         <li class="nav-item">
                            <a href="/src/marketing/productlist" class="nav-link"><span class="pcoded-micon"><i
                                        class="feather icon-align-justify"></i></span><span class="pcoded-mtext">Product</span></a>
                        </li>
                        <li class="nav-item">
                            <a href="/src/marketing/sliderlist" class="nav-link"><span class="pcoded-micon"><i
                                        class="feather icon-align-justify"></i></span><span class="pcoded-mtext">Slider
                                    </span></a>
                        </li>
                        <li class="nav-item">
                            <a href="/src/marketing/postlist" class="nav-link"><span class="pcoded-micon"><i
                                        class="feather icon-align-justify"></i></span><span class="pcoded-mtext">Post
                                    </span></a>
                        </li>
                        <li class="nav-item pcoded-menu-caption">
                            <label>Chart & Maps</label>
                        </li>
                        <li class="nav-item">
                            <a href="chart-morris.html" class="nav-link"><span class="pcoded-micon"><i
                                        class="feather icon-pie-chart"></i></span><span
                                    class="pcoded-mtext">Chart</span></a>
                        </li>
                        <li class="nav-item">
                            <a href="map-google.html" class="nav-link"><span class="pcoded-micon"><i
                                        class="feather icon-map"></i></span><span class="pcoded-mtext">Maps</span></a>
                        </li>
                        <li class="nav-item pcoded-menu-caption">
                            <label>Pages</label>
                        </li>
                        <li class="nav-item pcoded-hasmenu">
                            <a href="#!" class="nav-link"><span class="pcoded-micon"><i
                                        class="feather icon-lock"></i></span><span
                                    class="pcoded-mtext">Authentication</span></a>
                            <ul class="pcoded-submenu">
                                <li class=""><a href="auth-signup.html" class="" target="_blank">Sign up</a></li>
                                <li class=""><a href="auth-signin.html" class="" target="_blank">Sign in</a></li>
                            </ul>
                        </li>
                        <li class="nav-item"><a href="sample-page.html" class="nav-link"><span class="pcoded-micon"><i
                                        class="feather icon-sidebar"></i></span><span class="pcoded-mtext">Sample
                                    page</span></a></li>
                        <li class="nav-item disabled"><a href="#!" class="nav-link"><span class="pcoded-micon"><i
                                        class="feather icon-power"></i></span><span class="pcoded-mtext">Disabled
                                    menu</span></a></li>
                    </ul>
                    <div class="card text-center">
                        <div class="card-block">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="feather icon-sunset f-40"></i>
                            <h6 class="mt-3">Upgrade to pro</h6>
                            <p>upgrade for get full themes and 30min support</p>
                            <a href="https://codedthemes.com/item/flash-able-bootstrap-admin-template/" target="_blank"
                               class="btn btn-gradient-primary btn-sm text-white m-0">Upgrade</a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <!-- [ navigation menu ] end -->

        <!-- [ Header ] start -->
        <header class="navbar pcoded-header navbar-expand-lg navbar-light headerpos-fixed">
            <div class="m-header">
                <a class="mobile-menu" id="mobile-collapse1" href="#!"><span></span></a>
                <a href="index.html" class="b-brand">
                    <img src="../assets/images/logo.svg" alt="" class="logo images">
                    <img src="../assets/images/logo-icon.svg" alt="" class="logo-thumb images">
                </a>
            </div>
            <a class="mobile-menu" id="mobile-header" href="#!">
                <i class="feather icon-more-horizontal"></i>
            </a>
            <div class="collapse navbar-collapse">
                <a href="#!" class="mob-toggler"></a>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <div class="main-search open">
                            <div class="input-group">
                                <input type="text" id="m-search" class="form-control" placeholder="Search . . .">
                                <a href="#!" class="input-group-append search-close">
                                    <i class="feather icon-x input-group-text"></i>
                                </a>
                                <span class="input-group-append search-btn btn btn-primary">
                                    <i class="feather icon-search input-group-text"></i>
                                </span>
                            </div>

                        </div>

                    </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li>
                        <div class="dropdown">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown"><i
                                    class="icon feather icon-bell"></i></a>
                            <div class="dropdown-menu dropdown-menu-right notification">
                                <div class="noti-head">
                                    <h6 class="d-inline-block m-b-0">Notifications</h6>
                                    <div class="float-right">
                                        <a href="#!" class="m-r-10">mark as read</a>
                                        <a href="#!">clear all</a>
                                    </div>
                                </div>
                                <ul class="noti-body">
                                    <li class="n-title">
                                        <p class="m-b-0">NEW</p>
                                    </li>
                                    <li class="notification">
                                        <div class="media">
                                            <img class="img-radius" src="../assets/images/user/avatar-1.jpg"
                                                 alt="Generic placeholder image">
                                            <div class="media-body">
                                                <p><strong>John Doe</strong><span class="n-time text-muted"><i
                                                            class="icon feather icon-clock m-r-10"></i>5 min</span></p>
                                                <p>New ticket Added</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="n-title">
                                        <p class="m-b-0">EARLIER</p>
                                    </li>
                                    <li class="notification">
                                        <div class="media">
                                            <img class="img-radius" src="../assets/images/user/avatar-2.jpg"
                                                 alt="Generic placeholder image">
                                            <div class="media-body">
                                                <p><strong>Joseph William</strong><span class="n-time text-muted"><i
                                                            class="icon feather icon-clock m-r-10"></i>10 min</span></p>
                                                <p>Prchace New Theme and make payment</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="notification">
                                        <div class="media">
                                            <img class="img-radius" src="../assets/images/user/avatar-3.jpg"
                                                 alt="Generic placeholder image">
                                            <div class="media-body">
                                                <p><strong>Sara Soudein</strong><span class="n-time text-muted"><i
                                                            class="icon feather icon-clock m-r-10"></i>12 min</span></p>
                                                <p>currently login</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="notification">
                                        <div class="media">
                                            <img class="img-radius" src="../assets/images/user/avatar-1.jpg"
                                                 alt="Generic placeholder image">
                                            <div class="media-body">
                                                <p><strong>Joseph William</strong><span class="n-time text-muted"><i
                                                            class="icon feather icon-clock m-r-10"></i>30 min</span></p>
                                                <p>Prchace New Theme and make payment</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="notification">
                                        <div class="media">
                                            <img class="img-radius" src="../assets/images/user/avatar-3.jpg"
                                                 alt="Generic placeholder image">
                                            <div class="media-body">
                                                <p><strong>Sara Soudein</strong><span class="n-time text-muted"><i
                                                            class="icon feather icon-clock m-r-10"></i>1 hour</span></p>
                                                <p>currently login</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="notification">
                                        <div class="media">
                                            <img class="img-radius" src="../assets/images/user/avatar-1.jpg"
                                                 alt="Generic placeholder image">
                                            <div class="media-body">
                                                <p><strong>Joseph William</strong><span class="n-time text-muted"><i
                                                            class="icon feather icon-clock m-r-10"></i>2 hour</span></p>
                                                <p>Prchace New Theme and make payment</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="noti-footer">
                                    <a href="#!">show all</a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown drp-user">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon feather icon-settings"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-notification">
                                <div class="pro-head">
                                    <img src="../assets/images/user/avatar-1.jpg" class="img-radius"
                                         alt="User-Profile-Image">
                                    <span>John Doe</span>
                                    <a href="auth-signin.html" class="dud-logout" title="Logout">
                                        <i class="feather icon-log-out"></i>
                                    </a>
                                </div>
                                <ul class="pro-body">
                                    <li><a href="#!" class="dropdown-item"><i class="feather icon-settings"></i>
                                            Settings</a></li>
                                    <li><a href="#!" class="dropdown-item"><i class="feather icon-user"></i> Profile</a>
                                    </li>
                                    <li><a href="message.html" class="dropdown-item"><i class="feather icon-mail"></i> My
                                            Messages</a></li>
                                    <li><a href="auth-signin.html" class="dropdown-item"><i class="feather icon-lock"></i>
                                            Lock Screen</a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </header>
        <!-- [ Header ] end -->
        <!-- [ Main Content ] start -->
        <section class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <div class="main-body">
                            <div class="page-wrapper">
                                <!-- [ breadcrumb ] start -->
                                <div class="page-header">
                                    <div class="page-block">

                                        <div class="dropdown float-right" style="display: inline-flex; padding-top: 10px;">
                                            <div>
                                                <a href="#addEmployeeModal"  class="btn btn-info add-new" data-toggle="modal" ><i class="fa fa-plus"></i> <span>Add New</span></a>
                                            </div>
                                            <div class="dropdown">
                                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Category
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" href="/src/marketing/productlist?status=${status}">All Category</a>
                                                    <c:forEach items="${lsCategory}" var="c">
                                                        <a class="dropdown-item" href="/src/marketing/productlist?status=${status}&category=${c.categoryName}">${c.categoryName}</a>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                            <div class="dropdown">
                                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Status
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" href="/src/marketing/productlist?category=${c.categoryName}">All Status</a>
                                                    <c:forEach items="${lsStatus}" var="c">
                                                        <a class="dropdown-item" href="/src/marketing/productlist?status=${c.value}&category=${category}">${c.key}</a>
                                                    </c:forEach>
                                                </div>

                                            </div>


                                        </div>
                                        <div class="row align-items-center">
                                            <div class="col-md-12">
                                                <div class="page-header-title">
                                                    <h5 class="m-b-10">Bootstrap Basic Tables</h5>
                                                </div>
                                                <ul class="breadcrumb">
                                                    <li class="breadcrumb-item"><a href="index.html"><i
                                                                class="feather icon-home"></i></a></li>
                                                    <li class="breadcrumb-item"><a href="#!">Bootstrap Table</a></li>
                                                    <li class="breadcrumb-item"><a href="#!">Basic Tables</a></li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- [ breadcrumb ] end -->
                                <!-- [ Main Content ] start -->
                                <div class="row">




                                    <div class="col-xl-12">
                                        <div class="card">

                                            <div class="card-body table-border-style">
                                                <div class="table-responsive">
                                                    <table id="example" class="table table-striped" cellspacing="0"
                                                           width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>ID</th>
                                                                <th>Product Name</th>
                                                                <th>Title</th>
                                                                <th>Brief</th>
                                                                <th>Thumbnail</th>
                                                                <th>Category</th>
                                                                <th>Price</th>
                                                                <th>Discount</th>
                                                                <th>Status</th>

                                                                <th style="text-align: center;">Actions</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody style="">
                                                            <c:forEach items="${lsProduct}" var="c">
                                                                <tr>
                                                                    <td>${c.productID}</td>

                                                                    <td>${c.productName}</td>
                                                                    <td>${c.title}</td>
                                                                    <td>${c.title}</td>
                                                                    <td> <img src="${c.thumbnail}" width="100px" height="100px" </td>
                                                                    <td>${c.categoryID.categoryName}</td>
                                                                    <td>${c.price}</td>
                                                                    <td><i><fmt:formatNumber type="number"  maxFractionDigits="0" value="${c.discount*100}"/>%</i></td>
                                                                    <td><c:if test="${c.status == true}">On Shopping</c:if>
                                                                        <c:if test="${c.status == false}">Out Of Stock</c:if></td>
                                                                        <td style="text-align: center; color: black; text-decoration: none">
                                                                            <a href="#viewEmployeeModal" data-toggle="modal"> View </a>|<a href="#editEmployeeModal" data-toggle="modal"> Edit </a>| 
                                                                            <a style="color: black;" href="productStatus?productID=${c.productID}&status=1">Show </a>| <a style="color: black;"  href="productStatus?productID=${c.productID}&status=0">Hide </a>
                                                                    </td>
                                                                </tr>
                                                            <div id="viewEmployeeModal${i}" class="modal fade">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">						
                                                                            <h4 class="modal-title">View Product</h4>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <label for="productID">Product ID</label>
                                                                                <input id="productID" name="productID" type="text" class="form-control" value="${c.productID}" readonly="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="productName">Product Name</label>
                                                                                <input id="productName" name="productName" type="text" class="form-control" value="${c.productName}" readonly="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="title">Title</label>
                                                                                <input id="title" name="title" type="text" class="form-control" value="${c.title}" readonly="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="thumbnail">Thumbnail</label>
                                                                                <img src="${c.thumbnail}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="description">Description</label>
                                                                                <textarea id="description" name="description" class="form-control"  readonly="">${c.description}"</textarea>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="price">Price</label>
                                                                                <input id="price" name="price" type="text" class="form-control" value="${c.price}" readonly="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="quantity">Quantity</label>
                                                                                <input id="quantity" name="quantity" type="text" class="form-control" value="${c.quantity}" readonly="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="price">Price</label>
                                                                                <input id="price" name="price" type="text" class="form-control" value="${c.price}" readonly="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="weight">Weight</label>
                                                                                <input id="weight" name="weight" type="text" class="form-control" value="${c.weight}" readonly="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="degree">Degree</label>
                                                                                <input id="degree" name="degree" type="text" class="form-control" value="${c.degree}" readonly="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="time">Time</label>
                                                                                <input id="time" name="time" type="text" class="form-control" value="${c.time}" readonly="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="createdate">Create Date</label>
                                                                                <input id="createdate" name="createdate" type="text" class="form-control" value="${c.createdate}" readonly="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="category">Category</label>
                                                                                <input id="category" name="category" type="text" class="form-control" value="${c.categoryID.categoryName}" readonly="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="status">Status</label>
                                                                                <input id="createdate" name="createdate" type="text" class="form-control" value="${c.status}" readonly="">
                                                                            </div>

                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div id="editEmployeeModal${i}" class="modal fade">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <form action="editproduct" method="post">
                                                                            <div class="modal-header">						
                                                                                <h4 class="modal-title">Edit Product</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <div class="form-group">
                                                                                    <label for="productID">Product ID</label>
                                                                                    <input id="productID" name="productID" type="text" value="${c.productID}" class="form-control" readonly="">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="productName">Product Name</label>
                                                                                    <input id="productName" name="productName" type="text" value="${c.productName}" class="form-control" required>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="title">Title</label>
                                                                                    <input id="title" name="title" type="text" value="${c.title}"  class="form-control" required>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="thumbnail">Thumbnail</label>
                                                                                    <input id="thumbnail" name="thumbnail" type="text" value="${c.thumbnail}"  class="form-control" required>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="description">Description</label>
                                                                                    <textarea id="description" name="description" class="form-control" required>${c.description}</textarea>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="price">Price</label>
                                                                                    <input id="price" name="price" type="text" value="${c.price}" class="form-control" required>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="quantity">Quantity</label>
                                                                                    <input id="quantity" name="quantity" type="text" value="${c.quantity}" class="form-control" required>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="discount">Discount</label>
                                                                                    <input id="discount" name="discount" type="text" value="${c.discount}" class="form-control" required>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="weight">Weight</label>
                                                                                    <input id="weight" name="weight" type="text" value="${c.weight}" class="form-control" required>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="degree">Degree</label>
                                                                                    <input id="degree" name="degree" type="text" value="${c.degree}" class="form-control" required>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="time">Time</label>
                                                                                    <input id="time" name="time" type="text" value="${c.time}" class="form-control" required>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="createdate">Create Date</label>
                                                                                    <input id="createdate" name="createdate" type="text" value="${c.createdate}" class="form-control" required>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="categoryID">Category</label>
                                                                                    <select id="categoryID" name="categoryID" class="form-select" aria-label="Default select example">
                                                                                        <c:forEach items="${lsCategory}" var="o">
                                                                                            <option value="${o.categoryID}" ${c.categoryID.categoryID==o.categoryID?"selected":""}>${o.categoryName}</option>
                                                                                        </c:forEach>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="status">Status</label>
                                                                                    <select id="status" name="status" class="form-select" aria-label="Default select example">
                                                                                        <option value="true">On Shopping</option>
                                                                                        <option value="false">Out Of Stock</option>
                                                                                    </select>
                                                                                </div>

                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                                                                                <input type="submit" class="btn btn-success" value="Edit">
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </c:forEach>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <!-- [ Main Content ] end -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div id="addEmployeeModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="addproduct" method="post">
                        <div class="modal-header">						
                            <h4 class="modal-title">Add Product</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">					
                            <div class="form-group">
                                <label for="productName">Product Name</label>
                                <input id="productName" name="productName" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input id="title" name="title" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="thumbnail">Thumbnail</label>
                                <input id="thumbnail" name="thumbnail" type="file" accept=".png,.jpg" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea id="description" name="description" class="form-control" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="price">Price</label>
                                <input id="price" name="price" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="quantity">Quantity</label>
                                <input id="quantity" name="quantity" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="discount">Discount</label>
                                <input id="discount" name="discount" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="weight">Weight</label>
                                <input id="weight" name="weight" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="degree">Degree</label>
                                <input id="degree" name="degree" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="time">Time</label>
                                <input id="time" name="time" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="createdate">Create Date</label>
                                <input id="createdate" name="createdate" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="categoryID">Category</label>
                                <select id="categoryID" name="categoryID" class="form-select" aria-label="Default select example">
                                    <c:forEach items="${lsCategory}" var="o">
                                        <option value="${o.categoryID}">${o.categoryName}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select id="status" name="status" class="form-select" aria-label="Default select example">
                                    <option value="true">On Shopping</option>
                                    <option value="false">Out Of Stock</option>
                                </select>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                            <input type="submit" class="btn btn-success" value="Add">
                        </div>
                    </form>
                </div>
            </div>
        </div>                                            

        <!-- Required Js -->
        <script src="/src/assests/js/vendor-all.min.js"></script>
        <script src="/src/assests/plugins/bootstrap4/js/bootstrap.min.js"></script>
        <script src="/src/assests/js/pcoded.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
                integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
        <script
            src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
        <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

        <!-- (Optional) Latest compiled and minified JavaScript translation files -->
        <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/i18n/defaults-*.min.js"></script>
        <script>
          
            $(document).ready(function () {
                $('#example').DataTable();
            });
        </script>
        <script
        src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
        <script
        src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>

    </body>

</html>
